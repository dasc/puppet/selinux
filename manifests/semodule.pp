# The selinux::semodule defined type
#
# @summary Mananges custom selinux modules on a system
#
# @example Basic usage
#   Selinux::Semodule {'local_nagios':
#       ensure => 'present',
#       source => 'puppet:///usr/share/selinux/targeted/local_nagios.pp',
#   }
#
# @param modulename
#   The name of the module.  A module file with the name `$modulename.pp` is created in /usr/share/selinux/targeted/
#
# @param [Enum['absent', 'present']] ensure
#   Determines if the module should be installed and active or not.
#
# @param source
#   The binary module file to install.
#
define selinux::semodule ($modulename = $title, Enum['absent', 'present'] $ensure = 'present', $source) {
    include ::selinux
    
    selmodule { "${modulename}":
	ensure => "${ensure}",
    }

    if $ensure == 'present' {
	file { "/usr/share/selinux/targeted/${modulename}.pp" :
	    ensure => 'file',
	    owner => 'root',
	    group => 'root',
	    mode => '0644',
	    source => "${source}",
	}
	exec {"/usr/sbin/semodule -i /usr/share/selinux/targeted/${modulename}.pp":
	    refreshonly => true,
	    subscribe => File["/usr/share/selinux/targeted/${modulename}.pp"],
	}
    } else {
	file { "/usr/share/selinux/targeted/${modulename}.pp" :
	    ensure => 'absent',
	}
    }
}
