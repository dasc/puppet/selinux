# The selinux class
#
# @summary Manages the selinux state on a system
#
# @example Basic usage
#   class {'::selinux':
#       package_ensure => 'installed',
#       policycoreutils_package_ensure => '2.5-29.el7',
#       mode => 'enforcing',
#       type => 'targeted',
#   }
#
# @param [String] package_ensure
#   The desired package state for the selinux packages, eg 'installed', or an explicit version number.
#
# @param [String] policycoreutils_package_ensure
#   The desired package state for the policycoreutils packages, eg 'installed', or an explicit version number.
#
# @param [Enum['enforcing', 'permissive', 'disabled']] mode
#   The desired selinux state on the host.  Defaults to `permissive` in hiera
#
# @param [Enum['targeted', 'minimum', 'mls']] type
#   The security scheme to enforce
#
class selinux (
    String $package_ensure,
    String $policycoreutils_package_ensure,
    String $policycoreutils_package,
    Enum['enforcing', 'permissive', 'disabled'] $mode,
    Enum['targeted', 'minimum', 'mls'] $type,
) {
    package {'selinux-policy': ensure => "${package_ensure}", }
    package {'selinux-policy-devel': ensure => "${package_ensure}", }
    package {'selinux-policy-targeted': ensure => "${package_ensure}", }
    package {"${policycoreutils_package}": ensure => "${policycoreutils_package_ensure}", }
    package {'policycoreutils-devel': ensure => "${policycoreutils_package_ensure}", }

    file {'/etc/selinux/config':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => epp('selinux/selinux_config.epp', {
            'mode' => $mode,
            'type' => $type,
        }),
    }
    exec {'/usr/sbin/setenforce':
        command     => "/usr/sbin/setenforce ${mode}",
        refreshonly => true,
        subscribe   => File['/etc/selinux/config'],
    }
}
