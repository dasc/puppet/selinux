# selinux

#### Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with selinux](#setup)
    * [What selinux affects](#what-selinux-affects)
    * [Beginning with selinux](#beginning-with-selinux)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
1. [Limitations - OS compatibility, etc.](#limitations)

## Description

Manage the selinux state on a host. This is intended for use on RHEL7 and later systems.

This module will transition a host between the 'disabled', 'permissive', and 'enforcing' selinux states.  It can also be used to install and enable custom policy modules.

## Setup

### What selinux affects

This module will invoke 'setenforce' to change the active policy state, and update /etc/sysconfig/selinux to ensure the state persists after a reboot.

Custom policy modules are installed into /usr/share/selinux/targeted/.  This puppet module will run 'semodule -i' to activate a selinux policy module after it is installed.

### Beginning with selinux

With no options specified, put selinux into permissive mode.

```
class {'::selinux': }
```

## Usage

Put selinux into enforcing mode, and use the MLS security policy:

```
class {'::selinux':
    package_ensure => 'installed',
    policycoreutils_package_ensure => '2.5-29.el7',
    mode => 'enforcing',
    type => 'mls',
}
```

Ensure a specific version of the selinux and policycoreutils packages:
```
class {'::selinux':
    package_ensure => '3.13.1-229.el7_6.6',
    policycoreutils_package_ensure => '2.5-29.el7',
    mode => 'enforcing',
    type => 'mls',
}
```

Install a custom policy module:

```
selinux::semodule {'local_nagios':
    ensure => 'present',
    source => 'puppet://puppet/filestore/local_nagios.pp',
}
```

## Reference


**Classes**

* [`selinux`](#selinux): Manages the selinux state on a system

**Defined types**

* [`selinux::semodule`](#selinuxsemodule): Mananges custom selinux modules on a system

## Classes

### selinux

The selinux class

#### Examples

##### Basic usage

```puppet
class {'::selinux':
    package_ensure => 'installed',
    policycoreutils_package_ensure => '2.5-29.el7',
    mode => 'enforcing',
    type => 'targeted',
}
```

#### Parameters

The following parameters are available in the `selinux` class.

##### `package_ensure`

Data type: `String`

The desired package state for the selinux packages, eg 'installed', or an explicit version number.

##### `policycoreutils_package_ensure`

Data type: `String`

The desired package state for the policycoreutils packages, eg 'installed', or an explicit version number.

##### `mode`

Data type: `Enum['enforcing', 'permissive', 'disabled']`

The desired selinux state on the host.  Defaults to `permissive` in hiera

##### `type`

Data type: `Enum['targeted', 'minimum', 'mls']`

The security scheme to enforce

## Defined types

### selinux::semodule

The selinux::semodule defined type

#### Examples

##### Basic usage

```puppet
Selinux::Semodule {'local_nagios':
    ensure => 'present',
    source => 'puppet:///usr/share/selinux/targeted/local_nagios.pp',
}
```

#### Parameters

The following parameters are available in the `selinux::semodule` defined type.

##### `modulename`

Data type: `Any`

The name of the module.  A module file with the name `$modulename.pp` is created in /usr/share/selinux/targeted/

Default value: $title

##### `ensure`

Data type: `Enum['absent', 'present']`

Determines if the module should be installed and active or not.

Default value: 'present'

##### `source`

Data type: `Any`

The binary module file to install.


## Limitations

This module is only tested to work on RHEL7-based systems.
